<%-- 
    Document   : modificar
    Created on : 06/04/2021, 04:22:18 PM
    Author     : Yao
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h2>Modificar Registro</h2>

        <form action="ProductosController?accion=actualizar" method="POST" autocomplete="off">
            
            <input id="id" name="id" type="hidden" value="<c:out value="${producto.id_producto}"/>" />
            
            <p>
                Código:
                <input id="codigo" name="codigo" type="text" value="<c:out value="${producto.codigo_producto}"/>" />               
            </p>
            <p>
                Nombre:
                <input id="nombre" name="nombre" type="text" value="<c:out value="${producto.nombre_producto}"/>" />               
            </p>
            <p>
                Precio:
                <input id="precio" name="precio" type="text" value="<c:out value="${producto.precio_producto}"/>" />               
            </p>
            <p>
                Existencia:
                <input id="existencia" name="existencia" type="text" value="<c:out value="${producto.existencia_producto}"/>" />               
            </p>
            
            <button id="guardar" name="guardar" type="submit">Guardar</button>
            
        </form>
        
    </body>
</html>
