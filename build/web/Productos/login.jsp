<%-- 
    Document   : Login
    Created on : 31/05/2021, 06:57:45 PM
    Author     : Yao
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mudanzas</title>
        <link href="../CSS/Login.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div id="cuadro">
            <form accion="InicioSesion" method="post">
                <img src="../imagen/IconUser.png" class="user" alt="avatar"/>
                <hr>
                <br/><br/>
                <label id="subtitulo1">Correo:</label>
                <br/><br/>
                <input type="email" class="entrada" name="correo"/>
                <br/><br/>
                <label id="subtitulo2">Password:</label>
                <br/><br/>
                <input type="password" class="entrada" name="pass"/>
                <a href="https://www.facebook.com/" target="_blank">¿Olvidaste tu contraseña?</a>
                <br/><br/>
                <input type="submit" value="Iniciar sesión" id="botonIniciar"/>  
                
            </form>
        </div>
        <div class="registro">
            <p class="Nocuenta">¿No tiene cuenta?</p>
            <a class="buttonRegistro" href="registro.jsp">Registrarse</a>
        </div>
    </body>
</html>
