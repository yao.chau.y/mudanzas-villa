<%-- 
    Document   : Login
    Created on : 31/05/2021, 06:57:45 PM
    Author     : Yao
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mudanzas</title>
        <link href="../CSS/Login.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div id="cuadro">
            <form>
                <p id="titulo">Crear Cuenta</p>
                <hr>
                <br/>
                <label id="subtitulo1">Correo:</label>
                <br/><br/>
                <input type="email" class="entrada"/>
                <br/><br/>
                <label id="subtitulo2">Nombre:</label>
                <br/><br/>
                <input type="text" class="entrada"/>
                <br/><br/>
                <label id="subtitulo3">Apellidos:</label>
                <br/><br/>
                <input type="text" class="entrada"/>
                <br/><br/>
                <label id="subtitulo4">Telefono:</label>
                <br/><br/>
                <input type="number" class="entrada"/>
                <br/><br/>
                <label id="subtitulo5">Direccion:</label>
                <br/><br/>
                <input type="text" class="entrada"/>
                <br/><br/>
                <label id="subtitulo6">DNI:</label>
                <br/><br/>
                <input type="number" class="entrada"/>
                <br/><br/>
                <label id="subtitulo7">Password:</label>
                <br/><br/>
                <input type="password" class="entrada"/>
                <br/><br/>
                <input type="submit" value="Registrar cuenta" id="botonIniciar"/>  
            </form>
        </div>
    </body>
</html>
