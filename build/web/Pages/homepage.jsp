<%-- 
    Document   : homepage
    Created on : 01/06/2021, 07:58:45 PM
    Author     : Yao
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mudanzas Villa</title>
        <link href="../CSS/header.css" rel="stylesheet" type="text/css"/>
        <link href="../CSS/slides.css" rel="stylesheet" type="text/css"/>
        <link href="../CSS/animate.css" rel="stylesheet" type="text/css"/>
        <link href="../CSS/seccion-homepage.css" rel="stylesheet" type="text/css"/>
        <link href="../CSS/footer.css" rel="stylesheet" type="text/css"/>
        <link rel="shortcut icon" href="../imagen/VillaLogo.PNG">
        <style>
            .welcome{
                padding-top: 0px;
            }
        </style>
    </head>
    <body>
        <header>
            <div class="ancho">
                <div class="logo">
                    <p><a href="homepage.jsp">Mudanzas Villa</p>
                </div>
                <nav>
                    <ul>
                        <li><a href="sobreNosotros.jsp">SOBRE NOSOTROS</a></li>
                        <li><a href="solicitarCotizacion.jsp">SOLICITAR COTIZACIÓN</a></li>
                        <li><a href="contactanos.jsp">CONTÁCTANOS</a></li>
                        <li><a href="login.jsp">LOG IN</a></li>
                        <li><a href="#">INTRANET</a></li>
                    </ul>
                </nav>
            </div>
        </header>
        

        <div class="slideshow-container">
            <div class="mySlides fade">
                <img src="../imagen/Slide 1.jpg" style="width:100%">
                <div class="text"><p class="animate__slideInUp">Te brindamos la seguridad que tú necesitas</p></div>
            </div>
            <div class="mySlides fade">
                <img src="../imagen/Slide 2.jpg" style="width:100%">
                <div class="text"><p class="animate__slideInUp">Estamos contigo para dar el siguiente paso</p></div>
            </div>
            <div class="mySlides fade">
                <img src="../imagen/Slide 3.jpg" style="width:100%">
                <div class="text"><p class="animate__slideInUp">Te acompañamos en este gran reto</p></div>
            </div>
        </div>        
                
        
        <section class="welcome">
            <section class="bienvenidos">
                <h2>Bienvenidos a Mudanzas Villa</h2>
                <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                </h3>
            </section>
            <section class="contenedor-columnas-servicios">
                <div class="columnasx2-servicios">
                    <img class="servicios" src="../imagen/residencia.jpg" alt="Residencia">
                    <h4>Mudanzas Residenciales</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
                    </p>
                </div>
                <div class="columnasx2-servicios">
                    <img class="servicios" src="../imagen/empresa.jpg" alt="Empresa">
                    <h4>Mudanzas Corporativas</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
                    </p>
                </div>

            </section>
        </section>

        <section class="banner-1">
            <h3>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."</h3>
        </section>    
        
        
        <section class="advantage">
            <section class="porqueElegir">
                <h2>¿Por qué elegir a Mudanzas Villa para el servicio de mudanza?</h2>
                <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                </h3>
            </section>
            <section class="contenedor-columnas-advantage">
                <div class="columnasx2-ventajas">
                    <p>1. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                    </p>
                    <p>2. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                    </p>
                    <p>3. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                    </p>
                    <p>4. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                    </p>
                </div>
                <div class="columnasx2-ventajas">
                    <p>5. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                    </p>
                    <p>6. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                    </p>
                    <p>7. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                    </p>
                    <p>8. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                    </p>
                </div>
                
            </section>
            
        </section>    
        
        <div class="irCotizar">
            <a class="cotizacion" href="solicitarCotizacion.jsp">Solicitar cotización</a>
        </div>
        
        
        <div class="footer-basic">
            <footer>
                <div class="social">
                    <a href="#"><img src="../imagen/facebook.png"></a>
                    <a href="#"><img src="../imagen/instagram.png"></a>
                    <a href="#"><img src="../imagen/twitter.png"></a>
                    <a href="#"><img src="../imagen/youtube.png"></a>
                </div>
                <ul class="list-inline">
                    <li class="list-inline-item"><a href="homepage.jsp">Mudanzas Villa</a></li>
                    <li class="list-inline-item"><a href="sobreNosotros.jsp">Sobre nosotros</a></li>
                    <li class="list-inline-item"><a href="solicitarCotizacion.jsp">Solicitar cotización</a></li>
                    <li class="list-inline-item"><a href="contactanos.jsp">Contáctanos</a></li>
                    <li class="list-inline-item"><a href="#">Términos</a></li>
                    <li class="list-inline-item"><a href="#">Políticas de privacidad</a></li>
                </ul>
                <p class="copyright">Mudanzas Villa © 2021</p>
            </footer>
        </div>
        
    <script src="../Javascript/slide-homepage.js" type="text/javascript"></script>
    </body>
</html>
