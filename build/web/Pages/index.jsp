<%-- 
    Document   : index
    Created on : 06/04/2021, 04:21:53 PM
    Author     : Yao
--%>

<%--
Esto solo es una prueba, no es parte del proyecto de la pagina Mudanzas
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
          
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Almacen</title>
    </head>
    <body>
        <h1>Productos</h1>
        
        <a href="ProductosController?accion=nuevo">Nuevo registro</a>
        
        <br></br>
        
        <table border="1" width="80%">
            <tread>
                <th>Código</th>
                <th>Nombre</th>
                <th>Precio</th>
                <th>Existencias</th>
                <th></th>
                <th></th>
            </tread>         
            
            <tbody>
                
                <c:forEach var="producto" items="${lista}">
                    <tr>
                        <td><c:out value="${producto.codigo_producto}" /></td>
                        <td><c:out value="${producto.nombre_producto}" /></td>
                        <td><c:out value="${producto.precio_producto}" /></td>
                        <td><c:out value="${producto.existencia_producto}" /></td>
                        <td><a href="ProductosController?accion=modificar&id=<c:out value="${producto.id_producto}" />">Modificar</a></td>
                        <td><a href="ProductosController?accion=eliminar&id=<c:out value="${producto.id_producto}" />">Eliminar</a></td>
                    </tr>
                </c:forEach>
                   
            </tbody>
            
            
            
        </table>
        
    </body>
</html>
