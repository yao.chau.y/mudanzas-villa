<%-- 
    Document   : Login
    Created on : 31/05/2021, 06:57:45 PM
    Author     : Yao
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mudanzas Villa</title>
        <link href="../CSS/header-login-register.css" rel="stylesheet" type="text/css"/>
        <link href="../CSS/login-register.css" rel="stylesheet" type="text/css"/>
        <link rel="shortcut icon" href="../imagen/Logo 2.0.png">
    </head>
    <body>
        <header>
            <div class="ancho">
                <div class="logo">
                    <p><a href="homepage.jsp">Mudanzas Villa</p>
                </div>
                <nav>
                    <ul>
                        <li><a href="sobreNosotros.jsp">SOBRE NOSOTROS</a></li>
                        <li><a href="solicitarCotizacion.jsp">SOLICITAR COTIZACIÓN</a></li>
                        <li><a href="contactanos.jsp">CONTÁCTANOS</a></li>
                        <li><a href="login.jsp">LOG IN</a></li>
                        <li><a href="#">INTRANET</a></li>
                    </ul>
                </nav>
            </div>
        </header>
        
        <div id="cuadro">
            <form action="../InicioSesion" method="POST">
                <img src="../imagen/IconUser.png" class="user" alt="avatar"/>
                <hr>
                <br/><br/>
                <label id="subtitulo1">Correo:</label>
                <br/><br/>
                <input type="email" class="entrada" name="correo"/>
                <br/><br/>
                <label id="subtitulo2">Password:</label>
                <br/><br/>
                <input type="password" class="entrada" name="pass"/>
                <a href="https://www.facebook.com/" target="_blank">¿Olvidaste tu contraseña?</a>
                <br/><br/>
                <input type="submit" value="Iniciar sesión" id="botonIniciar"/>  
                
            </form>
        </div>
        <div class="registro">
            <p class="Nocuenta">¿No tiene cuenta?</p>
            <a class="buttonRegistro" href="registro.jsp">Registrarse</a>
        </div>
        <br>
    </body>
</html>
