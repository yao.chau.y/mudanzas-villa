/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/*
Esto solo es una prueba, no es parte del proyecto de la pagina Mudanzas
*/


package modelo;

/**
 *
 * @author Yao
 */
public class Productos {
    
    private int id_producto, existencia_producto;
    private String codigo_producto, nombre_producto;
    private Double precio_producto;

    public Productos(int id_producto, String codigo_producto, String nombre_producto, Double precio_producto, int existencia_producto) {
        this.id_producto = id_producto;
        this.existencia_producto = existencia_producto;
        this.codigo_producto = codigo_producto;
        this.nombre_producto = nombre_producto;
        this.precio_producto = precio_producto;
    }
    
    public int getId_producto() {
        return id_producto;
    }

    public void setId_producto(int id_producto) {
        this.id_producto = id_producto;
    }

    public int getExistencia_producto() {
        return existencia_producto;
    }

    public void setExistencia_producto(int existencia_producto) {
        this.existencia_producto = existencia_producto;
    }

    public String getCodigo_producto() {
        return codigo_producto;
    }

    public void setCodigo_producto(String codigo_producto) {
        this.codigo_producto = codigo_producto;
    }

    public String getNombre_producto() {
        return nombre_producto;
    }

    public void setNombre_producto(String nombre_producto) {
        this.nombre_producto = nombre_producto;
    }

    public Double getPrecio_producto() {
        return precio_producto;
    }

    public void setPrecio_producto(Double precio_producto) {
        this.precio_producto = precio_producto;
    }
    
    
}
