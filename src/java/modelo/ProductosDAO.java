/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/*
Esto solo es una prueba, no es parte del proyecto de la pagina Mudanzas
*/

package modelo;

import config.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Yao
 */
public class ProductosDAO {

    Connection conexion;
    
    public ProductosDAO() {
        Conexion con = new Conexion();
        conexion = con.getConexion();
    }    
    
    public List<Productos> listarProductos(){
        
        PreparedStatement ps;
        ResultSet rs;
        
        List<Productos> lista = new ArrayList<>();
        
        try{
            ps = conexion.prepareStatement("SELECT id_producto,codigo_producto,nombre_producto,precio_producto,existencia_producto FROM productos");
            rs = ps.executeQuery();
            
            while(rs.next()){
                int id = rs.getInt("id_producto");
                String codigo = rs.getString("codigo_producto");
                String nombre = rs.getString("nombre_producto");
                Double precio = rs.getDouble("precio_producto");
                int existencia = rs.getInt("existencia_producto");
                
                Productos producto = new Productos(id, codigo, nombre, precio, existencia);
                
                lista.add(producto);

            }
            return lista;
            
        }catch(SQLException e2){
            System.out.println(e2.toString());
            return null;
        }
        
    }
    
    public Productos mostrarProductos(int _id){
        
        PreparedStatement ps;
        ResultSet rs;
        
        Productos producto = null;
        
        try{
            ps = conexion.prepareStatement("SELECT id_producto,codigo_producto,nombre_producto,precio_producto,existencia_producto FROM productos WHERE id_producto=?");
            ps.setInt(1, _id);
            rs = ps.executeQuery();
            
            while(rs.next()){
                int id = rs.getInt("id_producto");
                String codigo = rs.getString("codigo_producto");
                String nombre = rs.getString("nombre_producto");
                Double precio = rs.getDouble("precio_producto");
                int existencia = rs.getInt("existencia_producto");
                
                producto = new Productos(id, codigo, nombre, precio, existencia);
                
            }
            
            return producto;
            
        }catch(SQLException e3){
            System.out.println(e3.toString());
            return null;
        }
    }
    
    public boolean insertarProductos(Productos producto){
        
        PreparedStatement ps;
        
        try{
            ps = conexion.prepareStatement("INSERT INTO productos(codigo_producto,nombre_producto,precio_producto,existencia_producto) VALUES (?,?,?,?)");
            ps.setString(1, producto.getCodigo_producto());
            ps.setString(2, producto.getNombre_producto());
            ps.setDouble(3, producto.getPrecio_producto());
            ps.setInt(4, producto.getExistencia_producto());
            
            ps.execute();
            
            return true;
            
        }catch(SQLException e4){
            System.out.println(e4.toString());
            return false;
        }
    }
        
    public boolean actualizarProductos(Productos producto){
        
        PreparedStatement ps;
        
        try{
            ps = conexion.prepareStatement("UPDATE productos SET codigo_producto=?,nombre_producto=?,precio_producto=?,existencia_producto=? WHERE id_producto=?");
            ps.setString(1, producto.getCodigo_producto());
            ps.setString(2, producto.getNombre_producto());
            ps.setDouble(3, producto.getPrecio_producto());
            ps.setInt(4, producto.getExistencia_producto());
            ps.setInt(5, producto.getId_producto());
            
            ps.execute();
            
            return true;
            
        }catch(SQLException e5){
            System.out.println(e5.toString());
            return false;
        }
    }
    
    public boolean eliminarProductos(int _id){
        
        PreparedStatement ps;
        
        try{
            ps = conexion.prepareStatement("DELETE FROM productos WHERE id_producto=?");
            ps.setInt(1, _id);
            
            ps.execute();
            
            return true;
            
        }catch(SQLException e6){
            System.out.println(e6.toString());
            return false;
        }
    }
 
}
