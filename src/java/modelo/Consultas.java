/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import config.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Yao
 */
public class Consultas {
    
    Connection conexion;
    
    public Consultas() {
        Conexion con = new Conexion();
        conexion = con.getConexion();
    }
    
    public boolean autenticacion(String correo, String contrasena){
        PreparedStatement pst = null;
        ResultSet rs = null;
        
        try {
            String consulta = "select * from cliente where CorreoCliente = ? and ContraseñaCliente = ?";
            pst = conexion.prepareStatement(consulta,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            pst.setString(1, correo);
            pst.setString(2, contrasena);
            rs = pst.executeQuery();
            
            if(rs.absolute(1)){
                return true;
            }
            
        } catch (Exception e) {
            System.err.println("Error" + e);
        }finally{
            try {
                if(conexion != null) conexion.close();
                if(pst != null) pst.close();
                if(rs != null) rs.close();
            } catch (SQLException e) {
                System.err.println("Error" + e);
            }
        }
        
        return false;
    }
   
    public boolean registrar(String correo, String nombre, String apellidos, int telefono, String direccion, int dni, String contra){
        
        PreparedStatement pst = null;
        
        try {
            double fiveDigits = 10000 + Math.random() * 90000;
            String consulta = "insert into cliente(CodCliente,CorreoCliente,NombreCLiente,ApellidosCliente,TelefonoCliente,DireccionCliente,DNI,ContraseñaCliente) values(?,?,?,?,?,?,?,?)";
            pst = conexion.prepareStatement(consulta);
            pst.setInt(1, (int)fiveDigits);
            pst.setString(2, correo);
            pst.setString(3, nombre);
            pst.setString(4, apellidos);
            pst.setInt(5, telefono);
            pst.setString(6, direccion);
            pst.setInt(7, telefono);
            pst.setString(8, contra);
            
            if(pst.executeUpdate() == 1){
                return true;
            }
            
        } catch (Exception e) {
            System.out.println("Error" + e);
        }finally{
            try {
                if(conexion != null) conexion.close();
                if(pst != null) pst.close();
            } catch (SQLException e) {
                System.err.println("Error" + e);
            }
        }
        
        return false;
    }
    
}
