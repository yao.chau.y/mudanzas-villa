<%-- 
    Document   : Login
    Created on : 31/05/2021, 06:57:45 PM
    Author     : Yao
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mudanzas Villa</title>
        <link href="../CSS/header-login-register.css" rel="stylesheet" type="text/css"/>
        <link href="../CSS/login-register.css" rel="stylesheet" type="text/css"/>
        <link rel="shortcut icon" href="../imagen/Logo 2.0.png">
    </head>
    <body>
        <header>
            <div class="ancho">
                <div class="logo">
                    <p><a href="homepage.jsp">Mudanzas Villa</p>
                </div>
                <nav>
                    <ul>
                        <li><a href="sobreNosotros.jsp">SOBRE NOSOTROS</a></li>
                        <li><a href="solicitarCotizacion.jsp">SOLICITAR COTIZACIÓN</a></li>
                        <li><a href="contactanos.jsp">CONTÁCTANOS</a></li>
                        <li><a href="login.jsp">LOG IN</a></li>
                        <li><a href="#">INTRANET</a></li>
                    </ul>
                </nav>
            </div>
        </header>
        
        <div id="cuadro">
            <form accion="../RegistroUsuario" method="POST">
                <p id="titulo">Crear Cuenta</p>
                <hr>
                <br/>
                <label id="subtitulo1">Correo:</label>
                <br/><br/>
                <input type="email" class="entrada" name="correo"/>
                <br/><br/>
                <label id="subtitulo2">Nombre:</label>
                <br/><br/>
                <input type="text" class="entrada" name="nombre"/>
                <br/><br/>
                <label id="subtitulo3">Apellidos:</label>
                <br/><br/>
                <input type="text" class="entrada" name="apellidos"/>
                <br/><br/>
                <label id="subtitulo4">Telefono:</label>
                <br/><br/>
                <input type="number" class="entrada" name="telefono"/>
                <br/><br/>
                <label id="subtitulo5">Direccion:</label>
                <br/><br/>
                <input type="text" class="entrada" name="direccion"/>
                <br/><br/>
                <label id="subtitulo6">DNI:</label>
                <br/><br/>
                <input type="number" class="entrada" name="dni"/>
                <br/><br/>
                <label id="subtitulo7">Password:</label>
                <br/><br/>
                <input type="password" class="entrada" name="pass"/>
                <br/><br/>
                <input type="submit" value="Registrar cuenta" id="botonIniciar"/>  
            </form>
        </div>
        <br>
        <br>
    </body>
</html>
