<%-- 
    Document   : solicitarCotizacion
    Created on : 04/06/2021, 12:44:41 AM
    Author     : Yao
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mudanzas Villa</title>
        <link href="../CSS/header.css" rel="stylesheet" type="text/css"/>
        <link href="../CSS/footer.css" rel="stylesheet" type="text/css"/>
        <link href="../CSS/seccion-homepage.css" rel="stylesheet" type="text/css"/>
        <link href="../CSS/formulario.css" rel="stylesheet" type="text/css"/>
        <link rel="shortcut icon" href="../imagen/VillaLogo.PNG">
    </head>
    <body>
        <header>
            <div class="ancho">
                <div class="logo">
                    <p><a href="homepage.jsp">Mudanzas Villa</p>
                </div>
                <nav>
                    <ul>
                        <li><a href="sobreNosotros.jsp">SOBRE NOSOTROS</a></li>
                        <li><a href="solicitarCotizacion.jsp">SOLICITAR COTIZACIÓN</a></li>
                        <li><a href="contactanos.jsp">CONTÁCTANOS</a></li>
                        <li><a href="login.jsp">LOG IN</a></li>
                        <li><a href="#">INTRANET</a></li>
                    </ul>
                </nav>
            </div>
        </header>
        
        <section class="welcome">
            <section class="bienvenidos">
                <h2>Solicitar cotización</h2>
                <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                </h3>
            </section>
        </section>
        
        <section class="form-columnx2">
            <div class="formulario">
                <h4>Formulario de contacto</h4>
                <form action="action">
                    <input type="text" name="nombre" placeholder="Nombre" class="caja-1">
                    <input type="text" name="apellidos" placeholder="Apellidos" class="caja-1">
                    <input type="email" name="correo" placeholder="Correo electrónico" class="caja-1">
                    <input type="tel" name="telefono" placeholder="Teléfono" class="caja-1">
                    <input type="text" name="servicio" placeholder="Tipo de servicio" class="caja-1">
                    <input type="date" name="fecha" placeholder="Fecha del servicio" class="caja-1">
                    <input type="text" name="direccion-origen" placeholder="Direccion de origen" class="caja-3">
                    <input type="text" name="direccion-destino" placeholder="Direccion de destino" class="caja-3">
                    <textarea name="comentarios" id="" cols="30" rows="10" class="caja-2" placeholder="Lista de posibles enseres"></textarea>
                    <button type="submit" class="cotizacion">Enviar</button>
                </form>
            </div>
            <div class="contacto">
                <div class="left-side">
                    <h2>Contacto</h2>                 
                </div>
                <div class="left-side">
                    <h3>987654321</h3>                 
                </div>
                <div class="left-side">
                    <h3>(01)2323232</h3>                 
                </div>
                <div class="left-side">
                    <h4>mudanzasVilla@gmail.com</h4>                 
                </div>
                <div class="left-side">
                    <h3>Lunes-Viernes: 8:00 - 18:00</h3>                 
                </div>
                <div class="left-side">
                    <h3>Sábados: 8:00 - 13:00</h3>                 
                </div>
            </div>
        </section>
        
        
        <div class="footer-basic">
            <footer>
                <div class="social">
                    <a href="#"><img src="../imagen/facebook.png"></a>
                    <a href="#"><img src="../imagen/instagram.png"></a>
                    <a href="#"><img src="../imagen/twitter.png"></a>
                    <a href="#"><img src="../imagen/youtube.png"></a>
                </div>
                <ul class="list-inline">
                    <li class="list-inline-item"><a href="homepage.jsp">Mudanzas Villa</a></li>
                    <li class="list-inline-item"><a href="sobreNosotros.jsp">Sobre nosotros</a></li>
                    <li class="list-inline-item"><a href="solicitarCotizacion.jsp">Solicitar cotización</a></li>
                    <li class="list-inline-item"><a href="contactanos.jsp">Contáctanos</a></li>
                    <li class="list-inline-item"><a href="#">Términos</a></li>
                    <li class="list-inline-item"><a href="#">Políticas de privacidad</a></li>
                </ul>
                <p class="copyright">Mudanzas Villa © 2021</p>
            </footer>
        </div>
        
        
    </body>
</html>