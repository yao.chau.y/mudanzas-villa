<%-- 
    Document   : sobreNosotros
    Created on : 04/06/2021, 12:44:27 AM
    Author     : Yao
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mudanzas Villa</title>
        <link href="../CSS/header.css" rel="stylesheet" type="text/css"/>
        <link href="../CSS/footer.css" rel="stylesheet" type="text/css"/>
        <link rel="shortcut icon" href="../imagen/VillaLogo.PNG">
    </head>
    <body>
        <header>
            <div class="ancho">
                <div class="logo">
                    <p><a href="homepage.jsp">Mudanzas Villa</p>
                </div>
                <nav>
                    <ul>
                        <li><a href="sobreNosotros.jsp">SOBRE NOSOTROS</a></li>
                        <li><a href="solicitarCotizacion.jsp">SOLICITAR COTIZACIÓN</a></li>
                        <li><a href="contactanos.jsp">CONTÁCTANOS</a></li>
                        <li><a href="login.jsp">LOG IN</a></li>
                        <li><a href="#">INTRANET</a></li>
                    </ul>
                </nav>
            </div>
        </header>
        
        <img class="mantenimiento" src="../imagen/mantenimiento.png" alt="matenimiento" width="100%">
        
        
        
        
        
        
        
        <div class="footer-basic">
            <footer>
                <div class="social">
                    <a href="#"><img src="../imagen/facebook.png"></a>
                    <a href="#"><img src="../imagen/instagram.png"></a>
                    <a href="#"><img src="../imagen/twitter.png"></a>
                    <a href="#"><img src="../imagen/youtube.png"></a>
                </div>
                <ul class="list-inline">
                    <li class="list-inline-item"><a href="homepage.jsp">Mudanzas Villa</a></li>
                    <li class="list-inline-item"><a href="sobreNosotros.jsp">Sobre nosotros</a></li>
                    <li class="list-inline-item"><a href="solicitarCotizacion.jsp">Solicitar cotización</a></li>
                    <li class="list-inline-item"><a href="contactanos.jsp">Contáctanos</a></li>
                    <li class="list-inline-item"><a href="#">Términos</a></li>
                    <li class="list-inline-item"><a href="#">Políticas de privacidad</a></li>
                </ul>
                <p class="copyright">Mudanzas Villa © 2021</p>
            </footer>
        </div>
        
        
    </body>
</html>
