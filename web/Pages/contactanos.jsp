<%-- 
    Document   : contactanos
    Created on : 04/06/2021, 12:44:54 AM
    Author     : Yao
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mudanzas Villa</title>
        <link href="../CSS/header.css" rel="stylesheet" type="text/css"/>
        <link href="../CSS/footer.css" rel="stylesheet" type="text/css"/>
        <link href="../CSS/seccion-homepage.css" rel="stylesheet" type="text/css"/>
        <link href="../CSS/formulario.css" rel="stylesheet" type="text/css"/>
        <link rel="shortcut icon" href="../imagen/VillaLogo.PNG">
    </head>
    <body>
        <header>
            <div class="ancho">
                <div class="logo">
                    <p><a href="homepage.jsp">Mudanzas Villa</p>
                </div>
                <nav>
                    <ul>
                        <li><a href="sobreNosotros.jsp">SOBRE NOSOTROS</a></li>
                        <li><a href="solicitarCotizacion.jsp">SOLICITAR COTIZACIÓN</a></li>
                        <li><a href="contactanos.jsp">CONTÁCTANOS</a></li>
                        <li><a href="login.jsp">LOG IN</a></li>
                        <li><a href="#">INTRANET</a></li>
                    </ul>
                </nav>
            </div>
        </header>
        
        <section class="welcome">
            <section class="bienvenidos">
                <h2>Contáctanos</h2>
                <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                </h3>
            </section>
        </section>
        
        <section class="form-columnx2">
            <div class="formulario">
                <h4>Formulario de contacto</h4>
                <form action="action">
                    <input type="text" name="nombre" placeholder="Nombre" class="caja-1">
                    <input type="text" name="apellidos" placeholder="Apellidos" class="caja-1">
                    <input type="email" name="correo" placeholder="Correo electrónico" class="caja-1">
                    <input type="tel" name="telefono" placeholder="Teléfono" class="caja-1">
                    <input type="text" name="asunto" placeholder="Asunto" class="caja-1">
                    <textarea name="comentarios" id="" cols="30" rows="10" class="caja-2" placeholder="Descripción"></textarea>
                    <button type="submit" class="cotizacion">Enviar</button>
                </form>
            </div>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4541.3202825466415!2d-77.0519763321219!3d-12.015941998120319!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x428e90276ca96a02!2sCentro%20de%20Tecnolog%C3%ADas%20de%20Informaci%C3%B3n%20y%20Comunicaciones!5e0!3m2!1ses-419!2spe!4v1622826723422!5m2!1ses-419!2spe" width="600px" height="440"% style="border:0;" allowfullscreen="" loading="lazy"></iframe>
        </section>
      
        
        
        <div class="footer-basic">
            <footer>
                <div class="social">
                    <a href="#"><img src="../imagen/facebook.png"></a>
                    <a href="#"><img src="../imagen/instagram.png"></a>
                    <a href="#"><img src="../imagen/twitter.png"></a>
                    <a href="#"><img src="../imagen/youtube.png"></a>
                </div>
                <ul class="list-inline">
                    <li class="list-inline-item"><a href="homepage.jsp">Mudanzas Villa</a></li>
                    <li class="list-inline-item"><a href="sobreNosotros.jsp">Sobre nosotros</a></li>
                    <li class="list-inline-item"><a href="solicitarCotizacion.jsp">Solicitar cotización</a></li>
                    <li class="list-inline-item"><a href="contactanos.jsp">Contáctanos</a></li>
                    <li class="list-inline-item"><a href="#">Términos</a></li>
                    <li class="list-inline-item"><a href="#">Políticas de privacidad</a></li>
                </ul>
                <p class="copyright">Mudanzas Villa © 2021</p>
            </footer>
        </div>
        
        
    </body>
</html>
